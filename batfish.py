#!/usr/bin/env python

import sys
import logging
import warnings
import json
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    from pybatfish.client.commands import *
    from pybatfish.question.question import load_questions, list_questions
    from pybatfish.question import bfq
    from pybatfish.datamodel.flow import HeaderConstraints as header


def test_config_sanity(isFailed):
    ps = bfq.fileParseStatus().answer()
    logger.info(ps.frame())
    if len(bfq.parseWarning().answer().frame()) > 0:
        logger.warning(bfq.parseWarning().answer().frame())
        logger.error('Error parsing configs')
        isFailed = True

    undefined = bfq.undefinedReferences().answer().frame()
    if len(undefined) > 0:
        logger.error("\nFound undefined data structures:")
        logger.info(undefined)
        isFailed = True
    else:
        logger.info("\nNo undefined data structures found")

    # Find all unused data structures
    unused = bfq.unusedStructures().answer().frame()
    if len(unused) > 0:
        logger.warning("\nFound unused data structures:")
        logger.info(unused)
        isFailed = True
    else:
        logger.info("\nNo unused data structures found")

    return isFailed


def test_dataplane(isFailed, fromNode, checkMultipath=True):
    ips = bfq.ipOwners().answer().frame()
    loopbacks = ips[(ips['Interface'] == 'lo0.0') & (ips['Active'])]
    localIP = loopbacks[loopbacks['Node'] == fromNode]['IP'].values[0]
    leaves = set(loopbacks[loopbacks['Node'].str.contains('leaf')]['IP'])
    leaves.remove(localIP)
    spines = set(loopbacks[loopbacks['Node'].str.contains('spine')]['IP'])
    mpath = len(spines)

    #logging.info("Progress: Analyzing traceroute from Leaf-3 to Leaf-1 and Leaf-2")
    # Set up the resulting data structure
    troute = dict()
    for leaf in leaves:
        troute[leaf] = dict()

    # Build headers for traceroute flows
    for leaf in leaves:
        troute[leaf]['header'] = header(srcIps=localIP, dstIps=leaf)

    # Ask questions about traceroute
    for leaf, data in troute.items():
        troute[leaf]['trace'] = bfq.traceroute(startLocation=fromNode, headers=data['header']).answer()
        troute[leaf]['result'] = data['trace'].get('answerElements')[0]['rows'][0]['Traces'][0]['disposition']
        troute[leaf]['paths'] = data['trace'].get('answerElements')[0]['rows'][0]['Traces']
        troute[leaf]['hops'] = data['trace'].get('answerElements')[0]['rows'][0]['Traces'][0].get('hops', [])

    #print(troute.items()[0][1]['result'])

    # Now let's check that the traceroute behaves as we expect
    for leaf, data in troute.items():
        print(leaf + ' ' + data['result'])

        #print(json.dumps(data['paths'], indent=4, sort_keys=True))

        for path in data['paths']:
            print('Path:')
            for hop in path['hops']:
                print(hop['node']['name'])


    return isFailed


def test_controlplane(isFailed):
    # Define a list of Spine switches
    # spines = set(bfq.nodeProperties(nodes='.*spine.*').answer().frame()['Node'])
    # logging.info("Progress: Analyzing control plane properties")

    # Get all BGP session status for leaf nodes
    bgp = bfq.bgpSessionStatus().answer().frame()
    # print(bgp)
    # print(bfq.routes(nodes='.*spine-1').answer().frame())

    ips = bfq.ipOwners().answer().frame()
    loopbacks = ips[(ips['Interface'] == 'lo0.0') & (ips['Active'])]['IP'].values
    for i, lo in enumerate(loopbacks):
        loopbacks[i] = lo + '/32'

    routes = bfq.routes().answer().frame()
    nodes = routes['Node'].drop_duplicates().values
    for node in nodes:
        # print('Node: ' + node)
        nr = []
        for lo in loopbacks:
            n = routes['Node'] == node
            r = routes['Network'] == lo
            if len(routes[n & r]['Network'].drop_duplicates().values) > 0:
                nr.append(routes[n & r]['Network'].drop_duplicates().values[0])
            else:
                logger.error('Missing ' + lo + ' in ' + node + ' RIB')
                isFailed = True
        # print(nr)
    if not isFailed:
        logger.info("\nLoopback connectivity is OK")
    return isFailed


logger = logging.getLogger('batfish')
formatter = logging.Formatter('%(message)s')
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
fh = logging.FileHandler('batfish.log', mode='w')
fh.setLevel(logging.INFO)
fh.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)
logger.setLevel(logging.INFO)


bf_session.coordinatorHost = 'jenkins.vanyas.ru'
bf_logger.setLevel(logging.ERROR)

# network = bf_set_network('fabric')
load_questions()
bf_init_snapshot('./', name='fabric')
# bf_init_snapshot('prod', name='fabric')
bf_set_snapshot('fabric')

csFailed = test_config_sanity(False)
cpFailed = test_controlplane(csFailed)
#dpFailed = test_dataplane(cpFailed)

bf_delete_snapshot('fabric')
# bf_delete_network('fabric')

quit(cpFailed)
