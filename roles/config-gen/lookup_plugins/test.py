import ipamdc


lm = ipamdc.LookupModule()
print(lm.run(['asn'], {'inventory_hostname': 'dc1-pod1-leaf-2', 'spine_count': 2, 'mgmt_network': '11.11.11.0/24', 'lo_network': '22.22.22.0/24', 'ptp_fabric_network': '33.33.33.0/24', 'leaf_asn_start': 65101, 'spine_asn_start': 65001}))
