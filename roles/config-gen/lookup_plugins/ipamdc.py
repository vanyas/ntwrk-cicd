# python 3 headers, required if submitting to Ansible
from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible.errors import AnsibleError, AnsibleParserError
from ansible.plugins.lookup import LookupBase

from netaddr import IPNetwork

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display

    display = Display()




class LookupModule(LookupBase):

    def run(self, terms, variables=None, **kwargs):

        host = variables['inventory_hostname']
        name = host.split('-')
        dc = name[0]
        pod = name[1]
        role = name[2]
        number = int(name[3])
        addr_type = terms[0].split('-')[0]
        mgmt_network = IPNetwork(variables['mgmt_network'])
        lo_network = IPNetwork(variables['lo_network'])
        ptp_fabric_network = IPNetwork(variables['ptp_fabric_network'])
        ret =''
        if addr_type == 'mgmt_ip':
            # if role == 'leaf':
            #     ret = mgmt_network.ip + number
            # elif role == 'spine':
            #     ret = mgmt_network.ip + 255 - number
            ret = variables['ansible_host'] + '/' + str(IPNetwork(variables['mgmt_network']).prefixlen)
        elif addr_type == 'lo_ip':
            if role == 'leaf':
                lo_network.value += number
                lo_network.prefixlen = 32
                ret = lo_network
            elif role == 'spine':
                lo_network.value =  lo_network.value + 255 - number
                lo_network.prefixlen = 32
                ret = lo_network
        elif addr_type == "leaf_spine":
            spine_no = int(terms[0].split('-')[1])
            ip_base = (number - 1) * int(variables['spine_count']) * 2
            ip = ip_base + (spine_no - 1) * 2 + 1
            ptp_fabric_network.value += ip
            ptp_fabric_network.prefixlen = 31
            ret = ptp_fabric_network
        elif addr_type == "spine_leaf":
            leaf_no = int(terms[0].split('-')[1])
            ip_base = (leaf_no - 1) * int(variables['spine_count']) * 2
            ip = ip_base + (number - 1) * 2
            ptp_fabric_network.value += ip
            ptp_fabric_network.prefixlen = 31
            ret = ptp_fabric_network
        elif addr_type == 'bgp_peer_spine':
            spine_no = int(terms[0].split('-')[1])
            ip_base = (number - 1) * int(variables['spine_count']) * 2
            ip = ip_base + (spine_no - 1) * 2
            ret = ptp_fabric_network.ip + ip
        elif addr_type == 'bgp_peer_leaf':
            leaf_no = int(terms[0].split('-')[1])
            ip_base = (leaf_no - 1) * int(variables['spine_count']) * 2
            ip = ip_base + (number - 1) * 2 + 1
            ret = ptp_fabric_network.ip + ip
        elif addr_type == 'asn':
            if role == 'spine':
                ret = int(variables['spine_asn_start']) + number - 1
            elif role == 'leaf':
                ret = int(variables['leaf_asn_start']) + number - 1
        elif addr_type == 'peer_asn_spine':
            spine_no = int(terms[0].split('-')[1])
            ret = int(variables['spine_asn_start']) + spine_no - 1
        elif addr_type == 'peer_asn_leaf':
            leaf_no = int(terms[0].split('-')[1])
            ret = int(variables['leaf_asn_start']) + leaf_no - 1
        return [str(ret)]
