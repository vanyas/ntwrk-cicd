FROM python:2.7

ARG UID=1000
ARG GID=1000
ARG HOME=/tmp

RUN git clone https://github.com/batfish/pybatfish.git && \
 pip install ./pybatfish && \
 pip install netaddr && \
 pip install ansible && \
 ansible-galaxy install juniper.junos && \
 groupadd -g $GID jenkins && \
 useradd -u $UID -g jenkins -d $HOME jenkins && \
 mkdir -p /var/lib/jenkins/workspace && \
 chown -R jenkins:jenkins /var/lib/jenkins